Contributing to the Slixmpp project
===================================

To contribute, the preferred way is to commit your changes on some
publicly-available git repository (on a fork `on gitlab
<https://lab.louiz.org/poezio/slixmpp-omemo>`_ or on your own
repository) and to notify the developers with either:
 - a ticket `on the bug tracker <https://lab.louiz.org/poezio/slixmpp-omemo/issues/new>`_
 - a merge request `on gitlab <https://lab.louiz.org/poezio/slixmpp-omemo/merge_requests>`_
 - a message on `the channel <xmpp:slixmpp@muc.poez.io>`_
